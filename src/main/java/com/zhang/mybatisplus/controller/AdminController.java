package com.zhang.mybatisplus.controller;


import com.zhang.mybatisplus.entity.Admin;
import com.zhang.mybatisplus.service.AdminService;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Controller
@RequestMapping("//admin")
public class AdminController {

    @Resource
    AdminService adminService;


    public List getList(){
        List<Admin> list = adminService.list();
        return list;
    }

}

