package com.zhang.mybatisplus.controller;


import com.zhang.mybatisplus.entity.Product;
import com.zhang.mybatisplus.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Controller
@RequestMapping("/product")
@Api(tags = "商品控制器")
@ResponseBody
public class ProductController {

    @Resource
    private ProductService productService;

    @GetMapping("/list")
    @ApiOperation(value = "查询所有商品")
    public List getList(){
        List<Product> list = productService.list();
        return list;
    }

}

