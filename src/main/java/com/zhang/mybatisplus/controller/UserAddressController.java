package com.zhang.mybatisplus.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Controller
@RequestMapping("//userAddress")
public class UserAddressController {

}

