package com.zhang.mybatisplus.controller;


import com.zhang.mybatisplus.entity.Product;
import com.zhang.mybatisplus.entity.User;
import com.zhang.mybatisplus.service.ProductService;
import com.zhang.mybatisplus.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Controller
@RequestMapping("/user")
@Api(tags = "用户控制器")
@ResponseBody
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/list")
    @ApiOperation(value = "查询所有用户")
    public List getList(){
        List<User> list = userService.list();
        return list;
    }

}

