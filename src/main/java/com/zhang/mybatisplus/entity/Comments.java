package com.zhang.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    public class Comments implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增id
     */
        @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 用户名称
     */
      private String loginName;

      /**
     * 用户id
     */
      private Integer userId;

      /**
     * 商品id
     */
      private Integer productId;

      /**
     * 商品评价
     */
      private String content;


}
