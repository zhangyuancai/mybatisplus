package com.zhang.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Data
  @EqualsAndHashCode(callSuper = false)
@ApiModel(description = "商品")
    public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 主键
     */
        @TableId(value = "id", type = IdType.AUTO)
        @ApiModelProperty(value = "id")
      private Integer id;

      /**
     * 名称
     */
      @ApiModelProperty(value = "商品名称")
      private String name;

      /**
     * 描述
     */
      @ApiModelProperty(value = "商品描述")
      private String description;

      /**
     * 价格
     */
      @ApiModelProperty(value = "价格")
      private Float price;

      /**
     * 库存
     */
      @ApiModelProperty(value = "库存")
      private Integer stock;

      /**
     * 分类1
     */
      @ApiModelProperty(value = "分类信息1")
      private Integer categoryleveloneId;

      /**
     * 分类2
     */
      @ApiModelProperty(value = "分类信息2")
      private Integer categoryleveltwoId;

      /**
     * 分类3
     */
      @ApiModelProperty(value = "分类信息3")
      private Integer categorylevelthreeId;

      /**
     * 文件名称
     */
      @ApiModelProperty(value = "图片文件名称")
      private String fileName;


}
