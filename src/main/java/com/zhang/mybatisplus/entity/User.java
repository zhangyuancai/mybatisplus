package com.zhang.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Data
  @EqualsAndHashCode(callSuper = false)
@ApiModel(description = "用户")
    public class User implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 主键
     */
        @TableId(value = "id", type = IdType.AUTO)
        @ApiModelProperty(value = "id")
      private Integer id;

      /**
     * 登录名
     */
      @ApiModelProperty(value = "登录名")
      private String loginName;

      /**
     * 用户名
     */
      @ApiModelProperty(value = "实际用户名")
      private String userName;

      /**
     * 密码
     */
      @ApiModelProperty(value = "密码")
      private String password;

      /**
     * 性别(1:男 0：女)
     */
      @ApiModelProperty(value = "性别")
      private Integer gender;

      /**
     * 身份证号
     */
      @ApiModelProperty(value = "身份证")
      private String identityCode;

      /**
     * 邮箱
     */
      @ApiModelProperty(value = "邮箱")
      private String email;

      /**
     * 手机
     */
      @ApiModelProperty(value = "手机")
      private String mobile;

  @ApiModelProperty(value = "头像图片")
    private String fileName;

  @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

  @ApiModelProperty(value = "修改时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
      private LocalDateTime updateTime;


}
