package com.zhang.mybatisplus.mapper;

import com.zhang.mybatisplus.entity.UserAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {

}
