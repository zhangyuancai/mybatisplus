package com.zhang.mybatisplus.service;

import com.zhang.mybatisplus.entity.ProductCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
public interface ProductCategoryService extends IService<ProductCategory> {

}
