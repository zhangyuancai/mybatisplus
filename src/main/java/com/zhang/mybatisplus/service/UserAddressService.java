package com.zhang.mybatisplus.service;

import com.zhang.mybatisplus.entity.UserAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
public interface UserAddressService extends IService<UserAddress> {

}
