package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.Cart;
import com.zhang.mybatisplus.mapper.CartMapper;
import com.zhang.mybatisplus.service.CartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements CartService {

}
