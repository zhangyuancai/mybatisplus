package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.Comments;
import com.zhang.mybatisplus.mapper.CommentsMapper;
import com.zhang.mybatisplus.service.CommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class CommentsServiceImpl extends ServiceImpl<CommentsMapper, Comments> implements CommentsService {

}
