package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.OrderDetail;
import com.zhang.mybatisplus.mapper.OrderDetailMapper;
import com.zhang.mybatisplus.service.OrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements OrderDetailService {

}
