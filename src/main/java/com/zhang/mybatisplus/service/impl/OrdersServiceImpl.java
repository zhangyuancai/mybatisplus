package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.Orders;
import com.zhang.mybatisplus.mapper.OrdersMapper;
import com.zhang.mybatisplus.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

}
