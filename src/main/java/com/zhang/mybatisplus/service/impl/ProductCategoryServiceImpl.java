package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.ProductCategory;
import com.zhang.mybatisplus.mapper.ProductCategoryMapper;
import com.zhang.mybatisplus.service.ProductCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryMapper, ProductCategory> implements ProductCategoryService {

}
