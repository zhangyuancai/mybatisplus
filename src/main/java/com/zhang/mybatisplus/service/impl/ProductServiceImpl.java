package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.Product;
import com.zhang.mybatisplus.mapper.ProductMapper;
import com.zhang.mybatisplus.service.ProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements ProductService {

}
