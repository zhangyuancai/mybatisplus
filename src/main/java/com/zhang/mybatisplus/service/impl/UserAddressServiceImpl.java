package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.UserAddress;
import com.zhang.mybatisplus.mapper.UserAddressMapper;
import com.zhang.mybatisplus.service.UserAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress> implements UserAddressService {

}
