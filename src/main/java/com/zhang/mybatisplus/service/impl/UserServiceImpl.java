package com.zhang.mybatisplus.service.impl;

import com.zhang.mybatisplus.entity.User;
import com.zhang.mybatisplus.mapper.UserMapper;
import com.zhang.mybatisplus.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yuanCai
 * @since 2021-11-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
