package com.zhang.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zhang.mybatisplus.entity.Admin;
import com.zhang.mybatisplus.mapper.AdminMapper;
import com.zhang.mybatisplus.service.AdminService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class MybatisplusApplicationTests {

    @Resource
    AdminService adminService;
    @Resource
    AdminMapper adminMapper;

    @Test
    void contextLoads() {
//        List<Admin> list = adminService.list();
//        System.out.println(list);
        List<Admin> admins = adminMapper.selectList(new QueryWrapper<>());
        System.out.println(admins);
    }

}
